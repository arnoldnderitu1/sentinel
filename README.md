# Sentinel

Users and logs provide clues. Sentinel provides answers.

## What is Sentinel?
Sentinel is a service that helps you monitor and fix crashes in realtime. The server is in Python, but it contains a full API for sending events from any language, in any application.
